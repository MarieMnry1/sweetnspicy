# SweetNSpicy : 
Projet scolaire React

API utilisée : [The Meal DB](https://www.themealdb.com/api.php)

Réalisé en solo

------------------------------------------------------------------------------------------------------------------------------------------

# Getting Started with Create React App

This project was created with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

## Install the project locally

First, clone the project
Then, you've to run `npm install`

## `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

Thanks for your attention ⚡️🙂