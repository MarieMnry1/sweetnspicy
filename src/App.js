import "./App.css";
import Header from "./components/Header";
import Shop from "./components/Shop";

const API_KEY = process.env.REACT_APP_MEALDB_API_KEY;
// console.log(API_KEY)

function App() {
  return (
    <div>
      <Header></Header>
      <Shop></Shop>
    </div>
  );
}

export default App;
