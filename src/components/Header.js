import React from 'react';

class Header extends React.Component {
  render() {
    return <h1>Sweet'N'Spicy 🌱</h1>
  }
}

export default Header;