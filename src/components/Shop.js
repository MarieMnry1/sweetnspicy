import React, { useEffect, useState } from "react";
import Meal from "./Meal";

const Shop = () => {
  const [meals, setMeals] = useState([]);
  const [names, setNames] = useState([]);
  const [searchName, setSearchName] = useState([]);
  useEffect(() => {
    fetch("https://www.themealdb.com/api/json/v1/1/search.php?f=t")
      .then((res) => res.json())
      .then((data) => setMeals(data.meals));
  }, []);

  useEffect(() => {
    fetch("https://www.themealdb.com/api/json/v1/1/search.php?f=" + searchName)
      .then((res) => res.json())
      .then((data) => setMeals(data.meals));
  }, [searchName]);

  // Handle search input
  const handleSearch = (event) => {
    const searchText = event.target.value;
    if (searchText === "") {
      console.log("not found");
    } else {
      setSearchName(searchText);
    }
  };
  // Handle button
  const handleBtn = (product) => {
    const newProduct = [...names, product];
    setNames(newProduct);
  };
  return (
    <div className="container">
      <div className="input-group mb-3">
        <input
          type="text"
          className="form-control"
          onChange={handleSearch}
          placeholder="Search Meals"
          aria-label="Recipient's username"
          aria-describedby="button-addon2"
        />
      </div>
      <div className="row">
        <div className="col-md-12">
          {meals.map((meal) => (
            <Meal key={meal.idMeal} handleBtn={handleBtn} meal={meal}></Meal>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Shop;
